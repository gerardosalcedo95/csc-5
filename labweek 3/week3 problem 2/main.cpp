/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 11, 2015, 4:02 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int singles, doubles, triples, homeruns, atbats;
    
    cout << "Please input data" << endl;
    
    cout << "singles: " << endl;
    cin >> singles;
    
    cout << "doubles: " << endl;
    cin >> doubles;
    
    cout << "triples: " << endl;
    cin >> triples;
    
    cout << "homeruns: " << endl;
    cin >> homeruns;
    
    cout << "atbats: " << endl;
    cin >> atbats;
    
    double slugging;
    slugging = (singles + (2.00*doubles) + (3*triples) + (4*homeruns)) / atbats;
    cout << "slugging: " << slugging;
    return 0;
}

