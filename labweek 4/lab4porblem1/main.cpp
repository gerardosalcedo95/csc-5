/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 23, 2015, 4:00 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "input a number grade" << endl;
    int grade;
    cin >> grade;
    
    if(grade > 100 || grade > 90)
    {
        cout << "A";
    }
    else if (grade > 79 && grade < 90)
    {
        cout << "B";
    }
    else if (grade > 69 && grade < 79)
    {
        cout << "C";
    }
    else if (grade > 59 && grade < 69)
    {
        cout << "D";
    }
    else 
    {
        cout << "F";
    }
    return 0;
}

